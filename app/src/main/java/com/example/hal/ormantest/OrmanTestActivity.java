package com.example.hal.ormantest;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.orman.dbms.Database;
import org.orman.dbms.sqliteandroid.SQLiteAndroid;
import org.orman.mapper.MappingSession;
import org.orman.mapper.Model;

import java.util.List;

public class OrmanTestActivity extends AppCompatActivity {

    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orman_test);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView)findViewById(R.id.listview);
        Button btnInput = (Button)findViewById(R.id.button);

        final EditText editText_name = (EditText)findViewById(R.id.editText_name);
        final EditText editText_age = (EditText)findViewById(R.id.editText_age);

        Database db = new SQLiteAndroid(this, "test", 1);
        MappingSession.registerDatabase(db);
        MappingSession.registerEntity(HumanEntity.class);
        MappingSession.start();

        final List<HumanEntity> humanEntities = Model.fetchAll(HumanEntity.class);
        final ListViewAdapter adapter = new ListViewAdapter(this, Model.fetchAll(HumanEntity.class));

        listView.setAdapter(adapter);
        System.out.println("test");
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View view, int pos, long id) {

                HumanEntity clickEntities = humanEntities.get(pos);
                String msg = clickEntities.name + "\n" + clickEntities.age;

                new AlertDialog.Builder(OrmanTestActivity.this)
                        .setTitle("人物")
                        .setMessage(msg)
                        .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // 完了 button pressed
                            }
                        }).show();
            }
        });

        btnInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(OrmanTestActivity.this, "Saved",
                        Toast.LENGTH_SHORT).show();

                HumanEntity userEntity = new HumanEntity();
                userEntity.name = editText_name.getText().toString();
                userEntity.age  = Integer.parseInt(editText_age.getText().toString());
                userEntity.insert();
                }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_orman_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle click s on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}