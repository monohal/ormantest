package com.example.hal.ormantest;

/**
 * Created by HAL on 2016/06/04.
 */

import org.orman.mapper.Model;
import org.orman.mapper.annotation.Entity;
import org.orman.mapper.annotation.PrimaryKey;

@Entity
public class HumanEntity extends Model<HumanEntity> {

    // primary key の指定
    // 無いと update() などでエラーになる
    @PrimaryKey(autoIncrement = true)
    public int id;

    public String name;
    public int age;
}
