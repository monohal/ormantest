package com.example.hal.ormantest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HAL on 2016/06/04.
 */

public class ListViewAdapter extends BaseAdapter {
    Context context;
    List<HumanEntity> humanEntities;
    LayoutInflater layoutInflater = null;


    public ListViewAdapter(Context context, List<HumanEntity> humanEntities){
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.humanEntities = humanEntities;
    }

    @Override
    public int getCount() {
        return humanEntities.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.list_view_layout, parent,false);
        }

        ((TextView)convertView.findViewById(R.id.listlayout_tv1)).setText(String.valueOf(humanEntities.get(position).id));
        ((TextView)convertView.findViewById(R.id.listlayout_tv2)).setText(humanEntities.get(position).name);
        ((TextView)convertView.findViewById(R.id.listlayout_tv3)).setText(String.valueOf(humanEntities.get(position).age));

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }
}
